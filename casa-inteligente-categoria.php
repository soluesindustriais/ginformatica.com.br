<? $h1 = "Casa inteligente";
$title  = "Casa inteligente";
$desc = "Orce $h1, conheça os melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include('inc/head.php'); ?>

<body> <? include('inc/header.php'); ?> <main class="container"> <?= $caminho2 ?><div class="row">
            <div class="container">
                <section> <?php include_once('inc/casa-inteligente/casa-inteligente-buscas-relacionadas.php'); ?><div class="container">
                        <h1><?= $h1 ?></h1>
                        <article class="full">
                        <p>A casa inteligente está se tornando cada vez mais popular, e é importante que o consumidor saiba o que está comprando. Aqui estão cinco coisas para procurar em uma casa inteligente.</p>

<p>O dispositivo de controle: o dispositivo de controle é o coração da casa inteligente. Procure por um dispositivo que seja fácil de usar, com um display intuitivo. Ele deve ser capaz de controlar todos os dispositivos da casa.</p>

<p>Os sensores: os sensores devem ser capazes de detectar qualquer mudança na casa, como abertura de janelas ou portas, movimento e umidade.</p>

<p>O alarme: um bom alarme é essencial para manter a casa segura. Procure por um alarme integrado aos sensores, que pode ser ativado quando há qualquer movimento na casa.</p>

<p>A conectividade: a casa inteligente deve ser conectada à internet, de modo que você possa monitorar e controlar a casa à distância.</p>

<p>O custo: o custo da casa inteligente pode variar muito. Procure por um dispositivo que seja compatível com os seus dispositivos e que tenha um bom custo-benefício.</p>
                            <ul class="thumbnails-2"> <?php include_once('inc/casa-inteligente/casa-inteligente-categoria.php'); ?> </ul>
                        </article> <br class="clear"> </section>
            </div>
        </div>
    </main>
    </div>
    </div><!-- .wrapper --> <? include('inc/footer.php'); ?> </body>

</html>