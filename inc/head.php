<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/Thing" lang="pt-br">

<head>
    <meta charset="utf-8">
    <?php include 'inc/geral.php';
    include 'inc/jquery.php'; ?>

    <link rel="preload" href="css/style.css" as="style">
    <link rel="stylesheet" href="css/style.css">

    <link rel="preconnect" href="https://use.fontawesome.com">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">



    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <!-- Google Tag Manager -->
    <!-- <script>(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PRTXQHT');</script> -->
    <!-- End Google Tag Manager -->
    <?php include 'inc/fancy.php'; ?>
    <script src="js/lazysizes.min.js"></script>


</head>