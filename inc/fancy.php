<script>
$(document).ready(function() {
    if (typeof $.fancybox !== "undefined") {
        console.log("Fancybox carregado!");
        $(".lightbox").fancybox({});
    } else {
        console.error("Erro: Fancybox NÃO foi carregado corretamente.");
    }
});
</script>
<!-- Fancybox CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" />

<!-- Fancybox JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>