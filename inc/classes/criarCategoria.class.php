
<?php

/*====================
|___Marcelo Serra____|
|  Data: 28/08/2020  |
====================*/

class Categoria{

    private $categorias;


    public function setCategorias($array)
    {
        $this->categorias = $array;
    }

    public function getCategorias()
    {
        return $this->categorias;
    }


    private function criarPaginaRaiz($file, $string)
    {
        $trata = new TrataString();
        $stringUpperSemHifen = ucwords($trata->retiraHifen($string));
        $stringSemAcento = $trata->trataAcentos($string);
        $text =  

        '<? $local = "categoria"; $h1 = "'.$stringUpperSemHifen.'"; $title  = "'.$stringUpperSemHifen.'"; $desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo"; $key  = ""; include("inc/head.php");?>    <body> <? include("inc/header.php");?> <main class="container"> <?=$caminho2?><div class="row"><div class="container"> <section>  <?php include_once("inc/'.$stringSemAcento.'/'.$stringSemAcento.'-buscas-relacionadas.php");?><div class="container"> <h1><?=$h1?></h1> <article class="full"> <p>O mercado de <?=$h1?> é amplo e conta com produtos e serviços que podem ser úteis em     diversas aplicações. No Soluções Industriais, portal especializado na geração de negócios para o mercado B2B, é possível    encontrar as melhores empresas que atuam nesse segmento.</p><p>Além de receber um orçamento, você também poderá esclarecer suas    dúvidas referentes ao assunto. Saiba mais sobre <?=$h1?> e faça uma cotação.</p> <ul class="thumbnails-2"> <?php include_once  ("inc/'.$stringSemAcento.'/'.$stringSemAcento.'-categoria.php");?> </ul> </article> <br class="clear"> </section> </div> </ div></main> </div></div><!-- .wrapper --> <? include("inc/footer.php");?> </body> </html>'; 

        $this->writeText($file, $text);
    }


    private function writeText($file, $string)
    {       
        $handle = fopen( $file , "w");
        fwrite($handle, $string);
        fclose($handle);
    }



    private function checkFileExists($string)
    {
        $trata = new TrataString();
        $stringSemAcentos = $trata->trataAcentos($string);
        $file = $stringSemAcentos."-categoria.php";
        if (!file_exists("./".$file))
        {
        $this->criarPaginaRaiz($file, $string);
        }
    }

    private function checkDirExists($string)
    {
        $trata = new TrataString();
        $stringSemAcentos = $trata->trataAcentos($string);
        $file = $stringSemAcentos;
        if(!is_dir("./inc/".$file))
        {
        mkdir("./inc/".$file, 0777);
        }
    }

    public function createBreadcrumb()
    {
      $trata = new TrataString();
      $breadcrumbs = '<?php';

        foreach($this->categorias as $categoria)
        {

           $breadcrumb = '$caminho'.$trata->trataAcentos($trata->trocaHifenUnderline($categoria)).
           " = '<div class=\"bg-white border-intro\">"
           .'<div class="container mt-5 pt-5 pb-2">
           <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
           <li class="breadcrumb-item"><a href="#HIJACKING.$url.#HIJACKING">Início</a></li>
           <li class="breadcrumb-item"><a href="#HIJACKING.$url.#HIJACKING'.$trata->trataAcentos($categoria).'-categoria"> '.$trata->retiraHifen($categoria). '</a></li>
           <li class="breadcrumb-item active" aria-current="page"><strong>#HIJACKING.$h1.#HIJACKING</strong></li>
           </ol></nav></div>'."';";

           $breadcrumbs .= "\n\n".str_replace('#HIJACKING', "'",$breadcrumb)."\n\n";
        
        };

      $breadcrumbs = $breadcrumbs."\n".'?>';
      $pathBreadcrumb = 'inc/breadcrumb.php';

      if(!file_exists( $pathBreadcrumb ))
      {
        $this->writeText($pathBreadcrumb, $breadcrumbs);
      };
    }

    public function criarCategoria(){
        foreach($this->categorias as $categoria){
        $this->checkFileExists($categoria);
        $this->checkDirExists($categoria);
        }
    }
};

?>