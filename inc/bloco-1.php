
<? include('inc/vetDestaque.php'); ?>
<section class="section pt-5">
   <div class="container mt-0 pt-2">

   <h2 class="pt-5 text-center">Encontre as <span
                     class="text-primary font-weight-bold">melhores empresas</span>, tudo em um <span
                     class="text-primary font-weight-bold">único lugar!</span></h2>
   <h2 class="pb-5 text-center mt-5"><span
                     class="text-primary font-weight-bold">Busque</span> facilmente o que procura!</h2>
   <? include( 'inc/search-box.php' ); ?>

<div class="section pb-3 pt-5">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-12 text-center">
            <div class="section-title mb-4 pb-2">
               <h4 class="title mb-4">Conheça nossos produtos</h4>
               <p class="text-muted para-desc mx-auto mb-0">Receba cotações de
               <? 
                  $i = 0; 
                  $numCategorias = count($categorias->getCategorias())-1; 
                  foreach($categorias->getCategorias() as $categoria):
                     $categoriaSemAcento = $trata->trataAcentos($categoria);
                     $categoriaSemHifen = $trata->retiraHifen($categoria);
                  ?>
               
                  <? if ( $i < $numCategorias ): ?>
                     <a href="<?=$categoriaSemAcento."-categoria"; ?>" class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?>, </a> 
                  <? else: ?>
                     <a href="<?=$categoriaSemAcento."-categoria"; ?>" class="text-primary font-weight-bold cor-fonte"><?= $categoriaSemHifen; ?></a> 
                  <? endif; ?>

               <? $i++; endforeach; ?>
               agora mesmo gratuitamente! </p>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
      <!-- Partners End -->
      <div class="row">
         <div class="col-12 mt-4 pt-2">
            <div id="customer-testi" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">
                     <? foreach($vetDestaque as $palavras => $palavra): ?>
                     <div style="height: 280px; max-height: 100%;" class="owl-item overflow-hidden position-relative products-carousel-home">
                        <a href="<?= $trata->trataAcentos(($palavra['palavra'])); ?>">
                           <div class="card customer-testi border-0 text-center position-relative bg-transparent">
                              <img class="w-100 mw-100 position-absolute absolute-top absolute-left" src="imagens/home/destaque/<?= $trata->trataAcentos(($palavra['palavra'])); ?>.jpg" alt="<?=$trata->retiraHifen($palavra['palavra']);?>">
                              <div class="card-body position-relative overflow-hidden bg-transparent">
                                <!--  <p class="d-none text-dark mt-1"><?=$palavra['texto'];?></p> -->
                                 <h3 class="text-primary text-center mt-3"><?=$trata->retiraHifen($palavra['palavra']);?></h3>
                              </div>
                           </div>
                        </a>
                     </div>
                     <? endforeach; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
     
     
     
     <!-- CARROSSEL COM LOGO INIT
      <div class="row pb-5">
            <div class="container mb-5 py-5">

            <div id="customer-test" class="owl-carousel owl-theme owl-loaded owl-drag">
               <div class="owl-stage-outer">
                  <div class="owl-stage">

                     <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/instalacao-e-montagem-industrial/instalacao-e-montagem-industrial-01.jpg" class="avatar avatar-medium w-100" alt="instalacao-e-montagem">
                           </div>
                        </div>
                     </div>
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/manutencao-predial/manutencao-predial-01.jpg" class="avatar avatar-medium w-100" alt="manutencao-predial">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/painel-e-quadro-eletrico/painel-e-quadro-eletrico-02.jpg" class="avatar avatar-medium w-100" alt="painel-e-quadro-eletrico">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/servicos-para-camaras-frias/servicos-para-camaras-frias-02.jpg" class="avatar avatar-medium w-100" alt="servicos-para-camaras-frias">
                           </div>
                        </div>
                     </div> 
                                          <div class="owl-item" >
                        <div class="card customer-testi border-0 text-center">
                           <div class="card-body">
                              <img src="imagens/servicos-spda-para-raios/servicos-spda-para-raios-4.jpg" class="avatar avatar-medium w-100" alt="">
                           </div>
                        </div>
                     </div>                      
                  </div>
               </div>
            </div>
                  </div>
   </div> 
      CARROSSEL COM LOGO END-->  



<div class="row">    
<h2 class="col-12 w-100 text-center py-5">Vantagens</h2>
         <div class="col-md-4 col-12">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/user.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Cote com diversas empresas</h4>
                  <p class="text-muted mb-0">Solicite orçamentos com vários fornecedores ao mesmo tempo e em poucos cliques.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/calendar.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Dezenas de cotações diariamente</h4>
                  <p class="text-muted mb-0">Solicite orçamento com dezenas de fornecedores qualificados de forma simples.</p>
               </div>
            </div>
         </div>
         <!--end col-->
         <div class="col-md-4 col-12 mt-5 mt-sm-0">
            <div class="features text-center">
               <div class="image position-relative d-inline-block">
                  <img src="imagens/svg/sand-clock.svg" class="avatar avatar-large" alt="">
               </div>
               <div class="content mt-4">
                  <h4 class="title-2">Economize tempo</h4>
                  <p class="text-muted mb-0">Otimize seu tempo e selecione os melhores fornecedores.</p>
               </div>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <div class="container mt-100 mt-60">
      <div class="row align-items-center">
         <div class="col-lg-6 col-md-6">
            <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-01.jpg" class="img-fluid shadow rounded mw-100" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0]));?>">
         </div>
         <!--end col-->
         <div class="col-lg-6 col-md-6 mt-4 mt-sm-0 pt-2 pt-sm-0">
            <div class="section-title ml-lg-5">
               <h4 class="title mb-4"><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[0])); ?></h4>
               <p class="text-muted">A empresa atende preferencialmente as regiões: ABC Paulista e capital, além da Baixada Santista.Os locais que aderem ao aluguel de CFTV, podem ter mais proteção contra roubos nos ambientes que são filmados com a utilização desse sistema inovador de filmagem. </p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[0]); ?>-categoria" class="mt-3 text-primary">Saiba Mais </a>
            </div>
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <?php if(isset($categorias->getCategorias()[1])){ ?>
      <div class="container mt-100 mt-60">
         <div class="row align-items-center">
            <div class="col-lg-7 col-md-6 order-2 order-md-1 mt-4 mt-sm-0 pt-2 pt-sm-0">
               <div class="section-title mr-lg-5">
                  <h4 class="title mb-4"><?= $trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1])); ?></h4>
                  <p class="text-muted">Os produtos de segurança são utensílios que vem sendo amplamente requisitado pelos mais distintos segmentos de negócio na atualidade, tendo em vista que, por meio deles, se tem a redução de uma forma considerável do número de furtos que acontece em um determinado local.</p>
               <a href="<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-categoria" class="mt-3 text-primary">Saiba Mais</a>
            </div>
         </div>
         <!--end col-->
         <div class="col-lg-5 col-md-6 order-1 order-md-2">
            <img src="imagens/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>/<?= $trata->trataAcentos($categorias->getCategorias()[1]); ?>-01.jpg" class="img-fluid shadow rounded mw-100" alt="<?=$trata->retiraHifen($trata->maiuscula($categorias->getCategorias()[1]));?>">
         </div>
         <!--end col-->
      </div>
      <!--end row-->
   </div>
   <!--end container-->
   <?php } ?>
