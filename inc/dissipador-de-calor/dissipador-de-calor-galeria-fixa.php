<div class="container p-0 my-3">
    <div class="row no-gutters justify-content-center py-3">
        <div class="col-lg-12 pt-2 mt-2 text-center">
            <div id="paginas-destaque" class="owl-carousel owl-theme owl-loaded owl-drag">
                <div class="col-12 py-2">
                    <div class="owl-stage-outer">
                        <div class="owl-stage"> <?php $lista = array('comprar dissipador de calor', 'dissipador aluminio metro', 'dissipador aluminio notebook', 'dissipador componente eletrônico', 'dissipador de alumínio para eletrônica', 'dissipador de alumínio para led', 'dissipador de calor', 'dissipador de calor 1 metro', 'dissipador de calor 10cm', 'dissipador de calor aluminio', 'dissipador de calor aluminio 1 metro', 'dissipador de calor alumínio preço', 'dissipador de calor barra', 'dissipador de calor de cobre', 'dissipador de calor em alumínio extrudado', 'dissipador de calor grande', 'dissipador de calor led', 'dissipador de calor notebook', 'dissipador de calor para amplificador', 'dissipador de calor pc', 'dissipador de calor placa de video', 'dissipador de calor placa mãe', 'dissipador de calor processador', 'dissipador de calor processador notebook', 'dissipador de calor sob medida', 'dissipador de calor sob medida especificada pelo comprador', 'dissipador de calor térmica', 'dissipador em aluminio', 'dissipador extrutado', 'dissipador para amplificador', 'dissipador para placa smd e tht', 'dissipador para transistor to 220', 'dissipadores de calor de componentes eletrônicos', 'dissipadores de calor para circuitos eletrônicos', 'dissipadores para luminária led', 'empresa de dissipador de calor', 'empresa de dissipador de calor aluminio', 'empresa de dissipador de calor de cobre', 'fabrica de dissipador de alumínio', 'fabricante de dissipadores de calor', 'fabricantes de dissipadores de alumínio', 'onde vende dissipador de calor', 'quanto custa dissipador de calor', 'quanto custa dissipador de calor aluminio', 'quanto custa dissipador de calor de cobre');
                                                shuffle($lista);
                                                for ($i = 1; $i < 13; $i++) { ?> <div class="owl-item">
                                    <div class="card blog rounded border-0 shadow overflow-hidden">
                                        <div class="position-relative border-intro"><a class="lightbox" href="<?= $url; ?>imagens/dissipador-de-calor/dissipador-de-calor-<?= $i ?>.webp" title="<?= $lista[$i] ?>"><img src="<?= $url; ?>imagens/dissipador-de-calor/thumbs/dissipador-de-calor-<?= $i ?>.webp" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
                                                <div class="overlay rounded-top bg-dark"></div>
                                                <div class="author"><small class="text-light user d-block"></small><small class="text-light date"><?= $lista[$i] ?></small></div>
                                        </div>
                                    </div></a>
                                </div><?php } ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>