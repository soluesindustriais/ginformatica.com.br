<div class="cd-hero">
    <ul class="cd-hero-slider autoplay">
        <li class="selected">
            <div class="cd-full-width">
                <h2>Mouse Gamer</h2>
                <p>Mouse gamer é um dispositivo fundamental para quem gosta de levar vantagem em jogos de computador. Trata-se de um acessório composto por sensores de alta qualidade, o que...</p>
                <a href="<?=$url?>mouse-gamer" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>HD Externo</h2>
                <p>O HD externo preço justo se trata de um dispositivo que armazena os dados do computador, e é muito indicado para quem precisa guardar um acervo grande de... </p>
                <a href="<?=$url?>hd-externo" class="cd-btn">Saiba mais</a>
            </div>

        </li>
        <li>
            <div class="cd-full-width">
                <h2>SSD 240GB</h2>
                <p>Ssd 240gb (também conhecido como Solid-state drive ou unidade de estado sólido, em português) é uma tecnologia de armazenamento caracterizada pelo...</p>
                <a href="<?=$url?>ssd-240gb" class="cd-btn">Saiba mais</a>
            </div>

        </li>
    </ul>
    <div class="cd-slider-nav">
        <nav>
            <span class="cd-marker item-1"></span>
            <ul>
                <li class="selected"><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
                <li><a href="#0"><i class="far fa-circle" aria-hidden="true"></i></a></li>
            </ul>
        </nav>
    </div>

</div>



        <!-- Hero End -->