<? $local = "categoria";
$h1 = "Perifericos";
$title  = "Perifericos";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php"); ?>

<body> <? include("inc/header.php"); ?> <main class="container"> <?= $caminho2 ?><div class="row">
            <div class="container">
                <section> <?php include_once("inc/perifericos/perifericos-buscas-relacionadas.php"); ?><div class="container">
                        <h1><?= $h1 ?></h1>
                        <article class="full">
                        <p>Periféricos são dispositivos adicionais que podem ser conectados a um computador, como um <a href="mouse-g403" 
title="Mouse G403" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Mouse</a>, <a href="teclado-para-pc" 
title="teclado" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Teclado</a>, <a href="mouse-pad-atacado" 
title="mouse pad atacado" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">mouse pad atacado</a>, <a href="webcam-4k" 
title="Câmera" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Câmera</a>, impressora e muitos outros. Os periféricos de entrada são usados para introduzir informações no computador, como dados digitados no teclado ou imagens capturadas pela câmera. Os periféricos de saída são usados para exibir informações para o usuário, como as imagens que aparecem na tela do monitor ou as páginas impressas pela impressora.</p>

<p>Alguns periféricos, como o mouse e o teclado, são essenciais para o funcionamento do computador. Outros, como a impressora e a câmera, são opcionais. Os periféricos de entrada e saída são conectados aos componentes do computador através de portas específicas. As portas de entrada são usadas para conectar periféricos de entrada, como o mouse e o teclado. As portas de saída são usadas para conectar periféricos de saída, como a impressora e a câmera.</p>

<p>A maioria dos periféricos usa uma interface padrão que permite que o computador reconheça o dispositivo e controle suas funções. A interface padrão mais comum é a conhecida interface USB. Outras interfaces populares incluem a interface FireWire e a interface Thunderbolt.</p>

<p>Os periféricos são importantes para o aumento da produtividade do usuário. O mouse, por exemplo, permite que o usuário movimente o cursor na tela e clique nos botões. O teclado permite que o usuário digite texto. A impressora permite que o usuário imprima documentos. A câmera permite que o usuário capture imagens.</p>

                            <ul class="thumbnails-2"> <?php include_once("inc/perifericos/perifericos-categoria.php"); ?> </ul>
                        </article> <br class="clear"> </section>
            </div>
            </ div>
    </main>
    </div>
    </div><!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>

<a href="mouse-g403" 
title="Mouse G403" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Mouse</a>

<a href="webcam-4k" 
title="Câmera" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Câmera</a>


<a href="teclado-para-pc" 
title="teclado" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">Teclado</a>

<a href="mouse-pad-atacado" 
title="mouse pad atacado" target="_blank" style="cursor: pointer; color: #006fe6;font-weight:bold;">mouse pad atacado</a>