<!-- Footer Start -->

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-12 mb-0 mb-md-4 pb-0 pb-md-2">
                <a class="logo-footer" href="#"><?=$nomeSite;?><span class="text-primary">.</span></a>
                <p class="mt-4">O conteúdo do texto desta página é de direito reservado. Sua reprodução, parcial ou
                    total, mesmo citando nossos links, é proibida sem a autorização do autor. Crime de violação de
                    direito autoral – artigo 184 do Código Penal – Lei 9610/98 - Lei de direitos autorais</p>
                <ul class="list-unstyled social-icon social mb-0 mt-4">
                    <li class="list-inline-item"><a rel="nofollow noopener"
                            href="https://www.facebook.com/solucoesindustriais/" target="_blank" class="rounded"><svg
                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-facebook fea icon-sm fea-social">
                                <path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
                            </svg></a></li>
                    <li class="list-inline-item"><a rel="nofollow noopener"
                            href="https://www.instagram.com/solucoesindustriais_/" target="_blank" class="rounded"><svg
                                xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                stroke-linejoin="round" class="feather feather-instagram fea icon-sm fea-social">
                                <rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
                                <path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
                                <line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
                            </svg></a></li>
                    <li class="list-inline-item"><a rel="nofollow noopener"
                            href="https://www.youtube.com/user/solucoesindustrial" target="_blank" class="rounded">

                            <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" width="24" height="24"
                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round"
                                class="feather feather-linkedin fea icon-sm fea-social">
                                <path
                                    d="M22.54 6.42a2.78 2.78 0 0 0-1.94-2C18.88 4 12 4 12 4s-6.88 0-8.6.46a2.78 2.78 0 0 0-1.94 2A29 29 0 0 0 1 11.75a29 29 0 0 0 .46 5.33A2.78 2.78 0 0 0 3.4 19c1.72.46 8.6.46 8.6.46s6.88 0 8.6-.46a2.78 2.78 0 0 0 1.94-2 29 29 0 0 0 .46-5.25 29 29 0 0 0-.46-5.33z">
                                </path>
                                <polygon points="9.75 15.02 15.5 11.75 9.75 8.48 9.75 15.02"></polygon>
                            </svg>
                        </a></li>
                    <li class="list-inline-item"><a rel="nofollow noopener"
                            href="https://www.linkedin.com/company/solucoesindustriais/" target="_blank"
                            class="rounded"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                stroke-linecap="round" stroke-linejoin="round"
                                class="feather feather-linkedin fea icon-sm fea-social">
                                <path
                                    d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z">
                                </path>
                                <rect x="2" y="9" width="4" height="12"></rect>
                                <circle cx="4" cy="4" r="2"></circle>
                            </svg></a></li>
                </ul>
                <!--end icon-->

            </div>
            <!--end col-->


            <div class="col-lg-4 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Produtos</h4>
                <ul class="list-unstyled footer-list mt-4">

                    <? foreach ($categorias->getCategorias() as $categoria):
                           $categoriaSemHifen = $trata->retiraHifen($categoria);
                           $categoriaSemAcento = $trata->trataAcentos($categoria);
                    ?>
                        <li>
                            <a href="<?=$categoriaSemAcento."-categoria"; ?>" class="text-foot"><?=$trata->capitalizar($categoriaSemHifen); ?>
                            </a>
                        </li>
                       <? endforeach; ?>
                    <li><a class="text-foot" href="<?=$url;?>mapa-site.php"> Mapa do site</a></li>
                </ul>
            </div>
            <!--end col-->


            <div class="d-none col-lg-3 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Informações</h4>
                <ul class="list-unstyled footer-list mt-4">
<!--                     <li><a href="" class="text-foot"> Palavra chave</a></li>
                    <li><a href="" class="text-foot"> Palavra chave</a></li>
                    <li><a href="" class="text-foot"> Palavra chave</a></li>
                    <li><a href="" class="text-foot"> Palavra chave</a></li>
                    <li><a href="" class="text-foot"> Palavra chave</a></li> -->
                </ul>
            </div>


            <!--end col-->


            <div class="col-lg-4 col-md-4 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                <h4 class="text-light footer-head">Anuncie seus produtos!</h4>
                <p class="mt-4">Aqui você pode solicitar cotações com empresas dos mais variados segmentos e ter acesso
                    a produtos qualificados. A diversidade de áreas atendidas pelo site garante que sua necessidade seja
                    atendida.</p>
                <form>
                    <div class="row">
                        <div class="col-lg-12">
                        </div>

                        <div class="col-lg-12">
                            <div class="buy-button">
                                <a rel="noopener noreferrer" id="#btnFooter" href="https://www.solucoesindustriais.com.br/participar-da-plataforma-hotsite" target="_blank"
                                    class="btn btn-primary">Gostaria de anunciar?</a>
                            </div>


                        </div>
                    </div>

                </form>
            </div>
            <!--end col-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</footer>
<!--end footer-->

<footer class="footer footer-bar">
    <div class="container text-center">
        <div class="row align-items-center">
            <div class="col-sm-12">
                <div
                    class="text-sm-center col-12 d-md-flex m-auto d-sm-block justify-content-center align-itens-center">
                    <img class="mw-75 logo-img" src="imagens/logo/logo-footer.png" alt="<? $nomeSite; ?>" title="<? $nomeSite; ?>">
                    <p class=" p-3 my-auto">© 2020 <?= $nomeSite; ?>. Um parceiro do Soluções Industriais </p>
                    <a title="logo" rel="noopener noreferrer" target="_blank" href="https://solucoesindustriais.com.br/">
                        <img src="imagens/logo/logo-solucs-white.png" alt="Soluções Industriais">
                    </a>
                </div>

            </div>
            <!--end col-->

        </div>
        <!--end row-->

    </div>
    <!--end container-->

</footer>

<a href="#" class="back-to-top rounded text-center" id="back-to-top" style="display: none;">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
        stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
        class="feather feather-chevron-up icons d-inline-block">
        <polyline points="18 15 12 9 6 15"></polyline>
    </svg>
</a>
<!-- Back to top -->

<?php
    include('inc/api-solucs.php');
    include('inc/addlightbox.php');
?>
<!-- <script src="js/jquery.fancybox.min.js"></script> -->

<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/scrollspy.min.js"></script>

<!-- SLIDER -->
<script src="js/owl.carousel.min.js"></script>
<script src="js/owl.init.js "></script>

<!-- Icons -->
<script src="js/feather.min.js"></script>

<!-- Main Js -->
<script src="js/app.js"></script>
<script src="hero/js/modernizr.js"></script>
<script src="hero/js/main.js"></script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRTXQHT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TXSHG615QE"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TXSHG615QE');
</script>
<!-- Google tag (gtag.js) --> <script async src="https://www.googletagmanager.com/gtag/js?id=G-D23WW3S4NC"></script> <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-D23WW3S4NC'); </script>
<!-- API SOLUÇÕES init-->

<!-- API SOLUÇÕES end-->



<!-- Default lazy Load Script -->
<script src="js/lazysizes.min.js"></script>
<script>
    const lazyContentImg = document.querySelectorAll('img');
    const lazyContentEmbed = document.querySelectorAll('embed');

    const insertDefaultLazy = (content) => 
    {
        let saveImgUrl = content.src;
        content.classList.add("lazyload");
        content.removeAttribute("src");
        content.setAttribute("data-src", saveImgUrl);
    };

    const insertNativeLazy = (content) => 
    {
        content.setAttribute("loading", "lazy");
    }

    lazyContentEmbed.forEach(index => insertNativeLazy(index));
    lazyContentImg.forEach(index => insertDefaultLazy(index));

</script>


    <!-- Script Launch start -->
    <script src="https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js" defer></script>
    <script>
        const aside = document.querySelector('aside');
        const data = '<div data-sdk-ideallaunch data-segment="Soluções Industriais - Oficial"></div>';
        aside != null ? aside.insertAdjacentHTML('afterbegin', data) : console.log("Não há aside presente para o Launch");
    </script>
    <!-- Script Launch end -->

<script type="module">
  import Typebot from 'https://cdn.jsdelivr.net/npm/@typebot.io/js@0.2.81/dist/web.js'

  Typebot.initBubble({
    typebot: "chatbotsatelites",
    prefilledVariables: {
            source: 'G Informática',
            URL: 'https://ginformatica.com.br', },
    apiHost: "https://chat.ferramentademarketing.com.br",
    previewMessage: {
      message:
        "Oi! Posso te ajudar?",
      autoShowDelay: 1000,
      avatarUrl:
        "https://s3.typebot.io/public/workspaces/clzir1det0001bsmim0a73co8/typebots/clzir2kub0005bsmicrxd9r3c/hostAvatar?v=1722968705385",
    },
    theme: {
      button: { backgroundColor: "#003ac2" },
      previewMessage: {
        backgroundColor: "#0042DA",
        textColor: "#FFFFFF",
        closeButtonBackgroundColor: "#0042DA",
        closeButtonIconColor: "#FFFFFF",
      },
      chatWindow: { backgroundColor: "#fff" },
    },
  });
</script>