<!-- page content -->
<div class="right_col" role="main">  
  <div class="row">
    <?php
    $lv = 3;
    if (!APP_USERS || empty($userlogin) || $user_level < $lv):
      die(WSErro("Desculpe, você não tem permissão para acessar esta área. <a href='javascript:history.back();' class='btn primary'>Voltar</a>", WS_ERROR, null, "Doutores da Web"));
    endif;
    ?>	
  </div>
  <div class="page-title">
    <div class="title col-md-12 col-sm-6 col-xs-12">
      <h3><i class="fa fa-database"></i> Importador de Dados</h3>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-12 col-sm-12 col-xs-12"> 
      <div class="x_panel">          

        <div class="x_title">
          <h2>Módulo para importar itens para o banco de dados<small>Aqui você pode definir parametros para importar informações através de um arquivo <b>.CSV</b> </small></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-down"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <form class="form-horizontal j_import" method="post" enctype="multipart/form-data">

            <input type="hidden" name="action" value="ImportCSV"/>
            <input type="hidden" name="baseDir" value="../../uploads/"/>

            <div class="x_panel">  
              <h2>Selecione um arquivo com extensão .csv para ser carregado na importação</h2>
              <p>Após selecionar o arquivo será apresentado a listagem onde será moldada a importação ao campos do banco de dados escolhido</p>
              <div class="clearfix"></div>
            </div>
            <br/>
            <br/>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tabela">Sessão de importação <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="cat_parent" class="form-control col-md-7 col-xs-12" required="required">
                  <?php
                  $ReadRecursos = new Read;
                  $ReadRecursos->ExeRead(TB_CATEGORIA, "WHERE user_empresa = :emp AND cat_parent IS NULL ORDER BY cat_title ASC", "emp={$_SESSION['userlogin']['user_empresa']}");
                  ?>
                  <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione --</option>
                  <?php if (!$ReadRecursos->getResult()): ?>
                    <option value="" class="text-danger" style="padding: 10px;" disabled="disabled">-- Cadastre um categoria antes de seguir --</option>
                    <?php
                  else:
                    foreach ($ReadRecursos->getResult() as $key):
                      ?>
                      <option value="<?= $key['cat_id'] ?>" style="padding:5px;">» <?= $key['cat_title']; ?></option>
                      <?php
                    endforeach;
                  endif;
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tabela">Importar para o Recurso de: <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <select name="tabela" class="form-control col-md-7 col-xs-12" required="required">
                  <option value="" selected="selected" class="text-info text-bold" style="padding: 10px;">-- Selecione a tabela --</option>
                  <?php foreach (Check::GetConstantes() as $key => $value): ?>
                    <option value="<?= $key; ?>"><?= $value; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="csv_file">Arquivo <span class="text-info">(.csv)</span> <span class="required">*</span></label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" id="app_file_csv" name="csv_file" required="required">
                <div class="clearfix"></div>
                <div class="j_progress none">
                  <div class="text-info">Progresso...</div>
                  <div class="progress">
                    <div class="progress-bar progress-bar-primary" data-transitiongoal="0"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-12">
                <button data-toggle="tooltip" data-placement="left" title="Carregar arquivo" type="submit" name="Cadastra" class="btn btn-primary"><i class="fa fa-upload"></i></button>              
              </div>
            </div>

          </form>
        </div>

      </div>

      <div id="callbacks"></div>

    </div>
  </div>
  <div class="clearfix"></div>
</div>
<!-- /page content -->