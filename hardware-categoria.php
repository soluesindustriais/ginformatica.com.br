<? $local = "categoria";
$h1 = "Hardware";
$title  = "Hardware";
$desc = "Orce $h1, conheça os    melhores fornecedores, compare hoje com aproximadamente 200 fabricantes ao mesmo tempo";
$key  = "";
include("inc/head.php"); ?>

<body> <? include("inc/header.php"); ?> <main class="container"> <?= $caminho2 ?><div class="row">
            <div class="container">
                <section> <?php include_once("inc/hardware/hardware-buscas-relacionadas.php"); ?><div class="container">
                        <h1><?= $h1 ?></h1>
                        <article class="full">
                        Hardware é tudo aquilo que constitui o computador, desde a CPU até os periféricos. É necessário que todo esse conjunto esteja em perfeito funcionamento para que o computador realize as tarefas desejadas.

<p>Porém, o hardware está sujeito a falhas e, por isso, é importante que o usuário conheça as principais peças do computador e saiba identificar eventuais problemas.</p>

<p>A CPU (Unidade Central de Processamento) é a peça mais importante do computador. Ela é responsável por realizar as tarefas informáticas, portanto, é importante que seja sempre protegida contra eventuais danos.</p>

<p>Os periféricos são itens essenciais para a conexão do usuário com o computador. São eles que permitem a entrada e a saída de informações. Os mais comuns são o teclado, o mouse, a webcam e o microfone.</p>

<p>A placa-mãe é a peça que liga todos os outros componentes do computador. Ela é responsável por gerenciar todo o sistema, além de conter os slots para a instalação de memória, placas de vídeo e outros periféricos.</p>

<p>A memória é um dos componentes mais importantes do computador. Ela é responsável por armazenar os dados que serão utilizados pela CPU. É importante escolher a memória correta, de acordo com as necessidades do computador.</p>

<p>A placa de vídeo é responsável pela exibição dos imagens na tela. É importante escolher uma placa de vídeo que seja compatível com o restante do hardware e com o sistema operacional utilizado.</p>
<!-- 
O disco rígido é o local onde são armazenados os arquivos do computador. É importante escolher um disco rígido que tenha espaço suficiente para armazenar todos os arquivos.

O processador é a peça que executa as instruções fornecidas pelo software. É importante escolher o processador adequado, de acordo com as necessidades do computador.

A fonte de alimentação é responsável por fornecer energia elétrica ao computador. É importante escolher uma fonte de alimentação adequada, de acordo com as necessidades do computador.

O cooler é responsável por resfriar a CPU. É importante escolher um cooler adequado, de acordo com as necessidades do computador.

O cabo de força é responsável por fornecer energia elétrica ao computador. É importante escolher um cabo de força adequado, de acordo com as necessidades do computador.

O monitor é responsável por exibir as imagens na tela. É importante escolher um monitor adequado, de acordo com as necessidades do computador.

O teclado é responsável pela entrada de dados. É importante escolher um teclado adequado, de acordo com as necessidades do computador.

O mouse é responsável pela entrada de dados. É importante escolher um mouse adequado, de acordo com as necessidades do computador. -->
                            <ul class="thumbnails-2"> <?php include_once("inc/hardware/hardware-categoria.php"); ?> </ul>
                        </article> <br class="clear"> </section>
            </div>
            </ div>
    </main>
    </div>
    </div><!-- .wrapper --> <? include("inc/footer.php"); ?> </body>

</html>